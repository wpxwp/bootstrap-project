$(document).ready(function() {
    $("#mycarousel").carousel( { interval: 2000 } );

    $('#carouselButton').click(function() {
        if($('#carouselButton').children('span').hasClass('fa-pause')) {
            $('#mycarousel').carousel('pause');
            $('#carouselButton span').removeClass('fa-pause');
            $('#carouselButton span').addClass('fa-play');
        } else if ($('#carouselButton').children('span').hasClass('fa-play')) {
            $('#mycarousel').carousel('cycle');
            $('#carouselButton').children('span').removeClass('fa-play');
            $('#carouselButton').children('span').addClass('fa-pause');
        };        
    });
});

// For Reservation Button
$('#reserveBtn').on('click', function(event) {
    event.preventDefault();
    $('#reserveModal').modal('toggle');
});

// For Login Button
$('#loginBtn').on('click', function(event) {
    event.preventDefault();
    $('#loginModal').modal('toggle');
});